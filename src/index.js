import promisize from 'promisize';
import 'source-map-support/register';

async function fetchFromContentUrl(contentUrl, client) {
  const downloadFile = promisize({
    target: client.file.download
  });

  const fileObj = await downloadFile(contentUrl);

  return fileObj;
}

async function fetchReferenceResources(refResourceName, client, filter = '') {
  const getReferenceResource = promisize({ target: client.getAll });
  const url = `resources/${refResourceName}${filter}`;

  const refResource = await getReferenceResource(url);

  return refResource;
}

// This function expects that the client/Ref combo returns a one item array or even one obj
export default async function downloadFileFromRefs(opts = {}) {
  const { urlPropPath, refResourceName, handler, client, filter = '' } = opts;
  let refResources = await fetchReferenceResources(refResourceName, client, filter);

  refResources = Array.isArray(refResources) ? refResources : [refResources];

  const loadedFiles = Promise.all(refResources.map(async res => {
    const contentUrl = res[urlPropPath];
    const downloadedFile = await fetchFromContentUrl(contentUrl, client);

    return handler(res, downloadedFile);
  }));

  return loadedFiles;
}
